<?php

namespace Markot\StawkavatBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Markot\StawkavatBundle\Entity\Stawkavat;
use Markot\StawkavatBundle\Form\StawkavatType;

/**
 * Stawkavat controller.
 *
 */
class StawkavatController extends Controller
{

    /**
     * Lists all Stawkavat entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MarkotStawkavatBundle:Stawkavat')->findAll();

        return $this->render('MarkotStawkavatBundle:Stawkavat:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Stawkavat entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Stawkavat();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('stawkavat_show', array('id' => $entity->getId())));
        }

        return $this->render('MarkotStawkavatBundle:Stawkavat:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Stawkavat entity.
    *
    * @param Stawkavat $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Stawkavat $entity)
    {
        $form = $this->createForm(new StawkavatType(), $entity, array(
            'action' => $this->generateUrl('stawkavat_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Stawkavat entity.
     *
     */
    public function newAction()
    {
        $entity = new Stawkavat();
        $form   = $this->createCreateForm($entity);

        return $this->render('MarkotStawkavatBundle:Stawkavat:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Stawkavat entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MarkotStawkavatBundle:Stawkavat')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Stawkavat entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MarkotStawkavatBundle:Stawkavat:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Stawkavat entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MarkotStawkavatBundle:Stawkavat')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Stawkavat entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MarkotStawkavatBundle:Stawkavat:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Stawkavat entity.
    *
    * @param Stawkavat $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Stawkavat $entity)
    {
        $form = $this->createForm(new StawkavatType(), $entity, array(
            'action' => $this->generateUrl('stawkavat_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Stawkavat entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MarkotStawkavatBundle:Stawkavat')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Stawkavat entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('stawkavat_edit', array('id' => $id)));
        }

        return $this->render('MarkotStawkavatBundle:Stawkavat:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Stawkavat entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MarkotStawkavatBundle:Stawkavat')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Stawkavat entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('stawkavat'));
    }

    /**
     * Creates a form to delete a Stawkavat entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('stawkavat_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
