<?php

namespace Markot\StawkavatBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MarkotStawkavatBundle:Default:index.html.twig', array('name' => $name));
    }
}
