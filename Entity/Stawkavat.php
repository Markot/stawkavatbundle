<?php

namespace Markot\StawkavatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stawkavat
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Stawkavat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nazwa", type="string", length=20)
     */
    private $nazwa;

    /**
     * @var float
     *
     * @ORM\Column(name="wartosc", type="decimal")
     */
    private $wartosc;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     * @return Stawkavat
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    
        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string 
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set wartosc
     *
     * @param float $wartosc
     * @return Stawkavat
     */
    public function setWartosc($wartosc)
    {
        $this->wartosc = $wartosc;
    
        return $this;
    }

    /**
     * Get wartosc
     *
     * @return float 
     */
    public function getWartosc()
    {
        return $this->wartosc;
    }
}
